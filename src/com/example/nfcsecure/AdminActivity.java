package com.example.nfcsecure;

import java.util.List;

import com.example.nfcsecure.models.NFCTransactionsDatasource;
import com.example.nfcsecure.models.Users;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class AdminActivity extends Activity {

	private NFCTransactionsDatasource datasource;
	static boolean IsConnectionOpen = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		datasource = new NFCTransactionsDatasource(getBaseContext());

		if (IsFirstTime())
			setContentView(R.layout.activity_admin);
		else {
			long LasttLoggedUser = LastLoggedUser();
			if (LasttLoggedUser > 0) {
				Intent mainActivity = new Intent(this, MainActivity.class);
				startActivity(mainActivity);
				finish();
			} else {
				Intent loginActivity = new Intent(this, LoginActivity.class);
				startActivity(loginActivity);
				finish();
			}
		}
	}

	@Override
	protected void onPause() {
		super.onPause();

		// ChattingFrame.activityPaused();
		datasource.close();
		IsConnectionOpen = false;
	}

	public void CreateUserAdmin(View view) {
		try {
			String username = ((EditText) findViewById(R.id.txtUsername)).getText().toString();
			String password = ((EditText) findViewById(R.id.txtPassword)).getText().toString();
			String confirmpassword = ((EditText) findViewById(R.id.txtConfirmPassword)).getText().toString();
			if (username.length() <= 5) {
				Toast.makeText(this, "Username should be longer than 5 letters !", Toast.LENGTH_SHORT).show();
				return;
			}

			if (password.length() <= 5) {
				Toast.makeText(this, "Password should be longer than 5 letters !", Toast.LENGTH_SHORT).show();
				return;
			}

			if (password.equals(confirmpassword) == false) {
				Toast.makeText(this, "Confirm Password not matching !", Toast.LENGTH_SHORT).show();
				return;
			}

			if (IsConnectionOpen == false)
				datasource.open();

			Users user = datasource.CreateUser(username, password, "Admin", 1, -1);
			if (user != null) {
				UpdateLastLoggedUser(user.getId());
				Intent mainActivity = new Intent(this, MainActivity.class);
				startActivity(mainActivity);
				finish();
			} else {
				Toast.makeText(this, "An Error occured while creating Admin user.", Toast.LENGTH_LONG).show();
			}

		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.admin, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private boolean IsFirstTime() {
		try {
			datasource.open();
			IsConnectionOpen = true;
			List<Users> users = datasource.getAllUsers();
			if (users.size() > 0)
				return false;
			else
				return true;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return false;
		}
	}

	private long LastLoggedUser() {
		SharedPreferences sharedpref = getSharedPreferences(getString(R.string.pref_key), Context.MODE_PRIVATE);

		// //////////////////////////////////////////////////
		// Temporary ONLY
		// SharedPreferences.Editor editor = sharedpref.edit();
		// // editor.putInt(getString(R.integer.IsVerified), 0);
		// editor.putBoolean("_IsRegistered", false);
		// editor.commit();
		// //////////////////////////////////////////////////
		long user = sharedpref.getLong("LastUserId", -1);
		return user;
	}

	private void UpdateLastLoggedUser(long UserId) {
		SharedPreferences sharedpref = getSharedPreferences(getString(R.string.pref_key), Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedpref.edit();
		editor.putLong("LastUserId", UserId);
		editor.commit();
	}

}
