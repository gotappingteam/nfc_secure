package com.example.nfcsecure;

import java.util.ArrayList;
import java.util.List;

import com.example.nfcsecure.models.NFCTransactionsDatasource;
import com.example.nfcsecure.models.Users;

import android.app.Activity;
import android.os.Bundle;
import android.provider.Contacts.Intents.UI;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.nfcsecure.models.Sites;
import com.example.nfcsecure.models.SpinnerObject;
import com.example.nfcsecure.models.Tags;

public class TagsActivity extends Activity {

	private NFCTransactionsDatasource datasource;
	static boolean IsConnectionOpen = false;
	Spinner spinSites;
	EditText txtTagID = null;
	EditText txtTagName = null;
	EditText txtTagUID = null;
	EditText txtTagContent = null;
	EditText txtTagDescription = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		try {

			txtTagID = (EditText) findViewById(R.id.txtTagID);
			txtTagName = (EditText) findViewById(R.id.txtName);
			txtTagUID = (EditText) findViewById(R.id.txtUID);
			txtTagContent = (EditText) findViewById(R.id.txtContent);
			txtTagDescription = (EditText) findViewById(R.id.txtDescription);

			datasource = new NFCTransactionsDatasource(getBaseContext());
			setContentView(R.layout.activity_tags);
			spinSites = ((Spinner) findViewById(R.id.spnSites));
			LoadList();

			// If Editing current tag
			Bundle b = getIntent().getExtras();
			long value = 0; // or other values
			if (b != null) {
				value = b.getLong("TagId");
				LoadTagInfo(value);
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	private void SaveTag() {
		try {

			String tagId = txtTagID.getText().toString();
			String tagName = txtTagName.getText().toString();
			String UID = txtTagUID.getText().toString();
			String Content = txtTagContent.getText().toString();
			String Description = txtTagDescription.getText().toString();

			long SiteID = (spinSites.getSelectedItemPosition() == -1 ? -1
					: ((SpinnerObject) spinSites.getSelectedItem()).getId());

			if (tagName.trim().length() < 3) {
				Toast.makeText(this, "Tag name should be minimum 3 letters !", Toast.LENGTH_SHORT).show();
				return;
			}

			if (UID.length() == 0) {
				Toast.makeText(this, "Tag should be scanned", Toast.LENGTH_SHORT).show();
				return;
			}

			try {
				if ("0".equals(txtTagID.getText())) {
					int status = datasource.UpdateTag(Long.parseLong(tagId), tagName, Content, UID, Description, 0);
					if (status > 0)
						Toast.makeText(this, "Tag saved succesffully.", Toast.LENGTH_SHORT).show();
					else
						Toast.makeText(this, "Error occured while saving tag details.", Toast.LENGTH_SHORT).show();

				} else {
					Tags tag = datasource.CreateTag(tagName, Content, UID, Description, SiteID);
					if (tag != null)
						Toast.makeText(this, "Tag created succesffully.", Toast.LENGTH_SHORT).show();
					else
						Toast.makeText(this, "Error occured while saving tag details.", Toast.LENGTH_SHORT).show();

				}
			} catch (Exception e) {
				Toast.makeText(this, "Error occured while saving tag details.", Toast.LENGTH_SHORT).show();
				e.printStackTrace();
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	private void ScanNFC() {

	}

	@Override
	protected void onPause() {
		super.onPause();

		// ChattingFrame.activityPaused();
		datasource.close();
		IsConnectionOpen = false;
	}

	private void LoadTagInfo(Long ID) {
		Tags tag = datasource.getTagByID(ID);
		if (tag != null) {
			txtTagID.setText(Long.toString(tag.getId()));
			txtTagName.setText(tag.getName());
			txtTagUID.setText(tag.getUId());
			txtTagContent.setText(tag.getContent());
			txtTagDescription.setText(tag.getDescription());
		}

	}

	private void LoadList() {
		try {
			if (IsConnectionOpen == false)
				datasource.open();

			List<Sites> sites = datasource.getAllSites();
			if (sites != null) {
				// database handler
				List<SpinnerObject> lables = new ArrayList<SpinnerObject>();
				// Creating adapter for spinner
				for (Sites siteObj : sites) {
					lables.add(new SpinnerObject(siteObj.getId(), siteObj.getName()));
				}
				ArrayAdapter<SpinnerObject> dataAdapter = new ArrayAdapter<SpinnerObject>(this,
						android.R.layout.simple_spinner_item, lables);
				// Drop down layout style - list view with radio button
				dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
				// attaching data adapter to spinner

				spinSites.setAdapter(dataAdapter);
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

	}
}
