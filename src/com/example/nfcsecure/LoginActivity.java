package com.example.nfcsecure;

import com.example.nfcsecure.models.NFCTransactionsDatasource;
import com.example.nfcsecure.models.Users;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends Activity {

	private NFCTransactionsDatasource datasource;
	static boolean IsConnectionOpen = false;
	EditText username = null;
	EditText password = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		
	}

	@Override
	protected void onPause() {
		super.onPause();
		// ChattingFrame.activityPaused();
		datasource.close();
		// IsConnectionOpen = false;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.login, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public void Login() {
		if (ValidateInputs() == false)
			return;

		if (CheckUser()) {
			Intent mainAct = new Intent(this, MainActivity.class);
			startActivity(mainAct);
			finish();
		} else {
			Toast.makeText(this, "Please verify your credentials !", Toast.LENGTH_SHORT).show();
		}
	}

	private boolean ValidateInputs() {
		try {
			if ((username.getText().length() > 0) || (username.getText().length() > 0)) {
				Toast.makeText(this, "Make sure you entered username and password!", Toast.LENGTH_SHORT).show();
				return false;
			} else
				return true;
		} catch (Exception ex) {
			ex.printStackTrace();
			Toast.makeText(this, "Make sure you entered username and password!", Toast.LENGTH_SHORT).show();
			return false;
		}

	}

	private boolean CheckUser() {
		datasource.open();
		String username = ((EditText) findViewById(R.id.txtUsername)).getText().toString();
		String password = ((EditText) findViewById(R.id.txtPassword)).getText().toString();

		Users user = datasource.getUserByCredential(username, password);
		if (user == null)
			return false;
		else
			return true;
	}
}
