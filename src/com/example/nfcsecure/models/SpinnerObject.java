package com.example.nfcsecure.models;

public class SpinnerObject {

	private long databaseId;
	private String databaseValue;

	public SpinnerObject(long l, String databaseValue) {
		this.databaseId = l;
		this.databaseValue = databaseValue;
	}

	public long getId() {
		return databaseId;
	}

	public String getValue() {
		return databaseValue;
	}

	@Override
	public String toString() {
		return databaseValue;
	}

}