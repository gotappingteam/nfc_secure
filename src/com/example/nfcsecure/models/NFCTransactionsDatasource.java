package com.example.nfcsecure.models;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class NFCTransactionsDatasource {
	private SQLiteDatabase database;
	private MySQLiteHelper dbHelper;

	private String[] sitesColumns = { MySQLiteHelper.COL_Sites_ID, MySQLiteHelper.COL_Sites_Name,
			MySQLiteHelper.COL_Sites_Code };

	private String[] tagsColumns = { MySQLiteHelper.COL_Tags_ID, MySQLiteHelper.COL_Tags_ID,
			MySQLiteHelper.COL_Tags_Name, MySQLiteHelper.COL_Tags_Content, MySQLiteHelper.COL_Tags_description,
			MySQLiteHelper.COL_Tags_SiteID, MySQLiteHelper.COL_Tags_UId };

	private String[] sessionsColumns = { MySQLiteHelper.COL_Sessions_ID, MySQLiteHelper.COL_Sessions_Days,
			MySQLiteHelper.COL_Sessions_Description, MySQLiteHelper.COL_Sessions_Duration,
			MySQLiteHelper.COL_Sessions_IsEnabled, MySQLiteHelper.COL_Sessions_SiteId,
			MySQLiteHelper.COL_Sessions_StartTime };
	private String[] sessionDetailsColumns = { MySQLiteHelper.COL_SessionDetails_ID,
			MySQLiteHelper.COL_SessionDetails_TagId, MySQLiteHelper.COL_SessionDetails_TagOrder,
			MySQLiteHelper.COL_SessionDetails_SessionId };

	private String[] transactionsColumns = { MySQLiteHelper.COL_Transactions_ID, MySQLiteHelper.COL_Transactions_Notes,
			MySQLiteHelper.COL_Transactions_SessionId, MySQLiteHelper.COL_Transactions_StepID,
			MySQLiteHelper.COL_Transactions_TransDateTime, MySQLiteHelper.COL_Transactions_UserID };

	private String[] usersColumns = { MySQLiteHelper.COL_Users_ID, MySQLiteHelper.COL_Users_DisplayName,
			MySQLiteHelper.COL_Users_IsAdmin, MySQLiteHelper.COL_Users_PhoneNumber };

	private Context contxt = null;

	public NFCTransactionsDatasource(Context context) {
		dbHelper = new MySQLiteHelper(context);
		contxt = context;
	}

	public void open() throws SQLException {
		database = dbHelper.getWritableDatabase();
	}

	public void close() {
		dbHelper.close();
	}

	///////////////////////////////////////////////////////////////////////////////////////////
	// Sites CRUD
	public Sites CreateSite(String name, String Code) {

		try {
			ContentValues values = new ContentValues();
			values.put(MySQLiteHelper.COL_Sites_Name, name);
			values.put(MySQLiteHelper.COL_Sites_Code, Code);

			long insertId = database.insert(MySQLiteHelper.TBL_Sites, null, values);

			Cursor cursor = database.query(MySQLiteHelper.TBL_Sites, sitesColumns,
					MySQLiteHelper.COL_Sites_ID + " = " + insertId, null, null, null, null);
			cursor.moveToFirst();
			Sites newSite = cursorToSite(cursor);
			cursor.close();
			return newSite;

		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}

	public List<Sites> getAllSites() {
		try {
			List<Sites> sites = new ArrayList<Sites>();
			Cursor cursor = database.query(MySQLiteHelper.TBL_Sites, sitesColumns, null, null, null, null, null);
			cursor.moveToFirst();
			try {
				while (!cursor.isAfterLast()) {
					Sites msg = cursorToSite(cursor);
					sites.add(msg);
					cursor.moveToNext();
				}

			} catch (Exception ex) {
				Log.e("getAllSites", ex.getMessage());
				return null;
			}
			cursor.close();
			return sites;
		} catch (Exception ex) {
			ex.printStackTrace();
			Log.e("getAllSites", ex.getMessage());
			return null;
		}
	}

	public Sites getSiteByID(Long siteId) {
		try {
			Sites site = new Sites();
			Cursor cursor = database.query(MySQLiteHelper.TBL_Sites, sitesColumns,
					" (" + MySQLiteHelper.COL_Sites_ID + " = " + siteId + " ) ", null, null, null,
					MySQLiteHelper.COL_Sites_ID + " DESC");
			cursor.moveToFirst();
			while (!cursor.isAfterLast()) {
				site = cursorToSite(cursor);
			}
			cursor.close();
			return site;

		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}

	public void deleteSite(long ID) {

		Log.i("Delete", "Site deleted with id: " + ID);
		database.delete(MySQLiteHelper.TBL_Sites, MySQLiteHelper.COL_Sites_ID + " = " + ID, null);
	}

	// Dynamic function to update Messages Status
	public Integer UpdateSite(Long ID, Long Name, Long Code) {
		ContentValues cv = new ContentValues();
		cv.put(MySQLiteHelper.COL_Sites_Name, Name);
		cv.put(MySQLiteHelper.COL_Sites_Code, Code);
		int rowupdated = database.update(MySQLiteHelper.TBL_Sites, cv, MySQLiteHelper.COL_Sites_ID + "=" + ID, null);
		return rowupdated;

	}

	private Sites cursorToSite(Cursor cursor) {
		Sites site = new Sites();
		site.setId(cursor.getLong(0));
		site.setName(cursor.getString(1));
		site.setCode(cursor.getString(2));
		return site;
	}
	///////////////////////////////////////////////////////////////////////////////////////////

	///////////////////////////////////////////////////////////////////////////////////////////
	// tags CRUD
	public Tags CreateTag(String Name, String Content, String UID, String Description, long SiteID) {
		try {
			ContentValues values = new ContentValues();
			values.put(MySQLiteHelper.COL_Tags_Name, Name);
			values.put(MySQLiteHelper.COL_Tags_UId, UID);
			values.put(MySQLiteHelper.COL_Tags_Content, Content);
			values.put(MySQLiteHelper.COL_Tags_description, Description);
			values.put(MySQLiteHelper.COL_Tags_SiteID, SiteID);

			long insertId = database.insert(MySQLiteHelper.TBL_Tags, null, values);

			Cursor cursor = database.query(MySQLiteHelper.TBL_Tags, tagsColumns,
					MySQLiteHelper.COL_Tags_ID + " = " + insertId, null, null, null, null);
			cursor.moveToFirst();
			Tags newTag = cursorToTag(cursor);
			cursor.close();
			return newTag;

		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}

	public List<Tags> getAllTags() {
		try {
			List<Tags> tags = new ArrayList<Tags>();
			Cursor cursor = database.query(MySQLiteHelper.TBL_Tags, tagsColumns, null, null, null, null, null);
			cursor.moveToFirst();
			try {
				while (!cursor.isAfterLast()) {
					Tags msg = cursorToTag(cursor);
					tags.add(msg);
					cursor.moveToNext();
				}

			} catch (Exception ex) {
				Log.e("getAllSites", ex.getMessage());
				return null;
			}
			cursor.close();
			return tags;
		} catch (Exception ex) {
			ex.printStackTrace();
			Log.e("getAllSites", ex.getMessage());
			return null;
		}
	}

	public List<Tags> getTagsBySiteID(Long siteId) {
		try {
			List<Tags> tags = new ArrayList<Tags>();
			Cursor cursor = database.query(MySQLiteHelper.TBL_Tags, tagsColumns,
					" (" + MySQLiteHelper.COL_Tags_SiteID + " = " + siteId + " ) ", null, null, null,
					MySQLiteHelper.COL_Sites_ID + " DESC");
			cursor.moveToFirst();
			while (!cursor.isAfterLast()) {
				Tags msg = cursorToTag(cursor);
				tags.add(msg);
				cursor.moveToNext();
			}
			cursor.close();
			return tags;

		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}

	public Tags getTagByID(Long tagId) {
		try {
			Tags tag = new Tags();
			Cursor cursor = database.query(MySQLiteHelper.TBL_Tags, tagsColumns,
					" (" + MySQLiteHelper.COL_Tags_ID + " = " + tagId + " ) ", null, null, null,
					MySQLiteHelper.COL_Tags_ID + " DESC");
			cursor.moveToFirst();
			while (!cursor.isAfterLast()) {
				tag = cursorToTag(cursor);
			}
			cursor.close();

			return tag;

		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}

	public void deleteTag(long ID) {

		Log.i("Delete", "Tag deleted with id: " + ID);
		database.delete(MySQLiteHelper.TBL_Tags, MySQLiteHelper.COL_Tags_ID + " = " + ID, null);
	}

	// Dynamic function to update Messages Status
	public Integer UpdateTag(Long ID, String Name, String Content, String UID, String Description, long SiteID) {
		ContentValues cv = new ContentValues();
		cv.put(MySQLiteHelper.COL_Tags_Name, Name);
		cv.put(MySQLiteHelper.COL_Tags_Content, Content);
		cv.put(MySQLiteHelper.COL_Tags_UId, UID);
		cv.put(MySQLiteHelper.COL_Tags_description, Description);
		cv.put(MySQLiteHelper.COL_Tags_SiteID, SiteID);

		int rowupdated = database.update(MySQLiteHelper.TBL_Tags, cv, MySQLiteHelper.COL_Tags_ID + "=" + ID, null);
		return rowupdated;

	}

	private Tags cursorToTag(Cursor cursor) {
		Tags tag = new Tags();
		tag.setId(cursor.getLong(0));
		tag.setName(cursor.getString(1));
		tag.setContent(cursor.getString(2));
		tag.setSiteId(cursor.getLong(3));
		tag.setDescription(cursor.getString(4));
		tag.setUId(cursor.getString(5));
		return tag;
	}

	///////////////////////////////////////////////////////////////////////////////////////////
	// sessions CRUD
	public Sessions CreateSession(String Days, String Description, long Duration, long IsEnabled, long SiteID,
			long StartTime) {
		try {
			ContentValues values = new ContentValues();
			values.put(MySQLiteHelper.COL_Sessions_Days, Days);
			values.put(MySQLiteHelper.COL_Sessions_Description, Description);
			values.put(MySQLiteHelper.COL_Sessions_Duration, Duration);
			values.put(MySQLiteHelper.COL_Sessions_IsEnabled, IsEnabled);
			values.put(MySQLiteHelper.COL_Sessions_SiteId, SiteID);
			values.put(MySQLiteHelper.COL_Sessions_SiteId, StartTime);

			long insertId = database.insert(MySQLiteHelper.TBL_Sessions, null, values);

			Cursor cursor = database.query(MySQLiteHelper.TBL_Tags, sessionsColumns,
					MySQLiteHelper.COL_Sessions_ID + " = " + insertId, null, null, null, null);
			cursor.moveToFirst();
			Sessions newSession = cursorToSession(cursor);
			cursor.close();
			return newSession;

		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}

	public List<Sessions> getAllSessions() {
		try {
			List<Sessions> sessions = new ArrayList<Sessions>();
			Cursor cursor = database.query(MySQLiteHelper.TBL_Tags, tagsColumns, null, null, null, null, null);
			cursor.moveToFirst();
			try {
				while (!cursor.isAfterLast()) {
					Sessions msg = cursorToSession(cursor);
					sessions.add(msg);
					cursor.moveToNext();
				}

			} catch (Exception ex) {
				Log.e("getAllSessions", ex.getMessage());
				return null;
			}
			cursor.close();
			return sessions;
		} catch (Exception ex) {
			ex.printStackTrace();
			Log.e("getAllSessions", ex.getMessage());
			return null;
		}
	}

	public Sessions getSessionByID(Long sessionId) {
		try {
			Sessions tag = new Sessions();
			Cursor cursor = database.query(MySQLiteHelper.TBL_Sessions, sessionsColumns,
					" (" + MySQLiteHelper.COL_Sessions_ID + " = " + sessionId + " ) ", null, null, null,
					MySQLiteHelper.COL_Sessions_ID + " DESC");
			cursor.moveToFirst();
			while (!cursor.isAfterLast()) {
				tag = cursorToSession(cursor);
			}
			cursor.close();
			return tag;

		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}

	public void deleteSession(long ID) {

		Log.i("Delete", "Sess deleted with id: " + ID);
		database.delete(MySQLiteHelper.TBL_Sessions, MySQLiteHelper.COL_Sessions_ID + " = " + ID, null);
	}

	// Dynamic function to update Messages Status
	public Integer UpdateSession(Long ID, String Days, String Description, long Duration, long IsEnabled, long SiteID,
			long StartTime) {
		ContentValues values = new ContentValues();
		values.put(MySQLiteHelper.COL_Sessions_Days, Days);
		values.put(MySQLiteHelper.COL_Sessions_Description, Description);
		values.put(MySQLiteHelper.COL_Sessions_Duration, Duration);
		values.put(MySQLiteHelper.COL_Sessions_IsEnabled, IsEnabled);
		values.put(MySQLiteHelper.COL_Sessions_SiteId, SiteID);
		values.put(MySQLiteHelper.COL_Sessions_SiteId, StartTime);

		int rowupdated = database.update(MySQLiteHelper.TBL_Tags, values, MySQLiteHelper.COL_Sessions_ID + "=" + ID,
				null);
		return rowupdated;

	}

	private Sessions cursorToSession(Cursor cursor) {
		Sessions session = new Sessions();
		session.setId(cursor.getLong(0));
		session.setDays(cursor.getString(1));
		session.setDescription(cursor.getString(2));
		session.setDuration(cursor.getLong(3));
		session.setIsEnabled(cursor.getLong(4));
		session.setStartTime(cursor.getLong(5));
		return session;
	}

	///////////////////////////////////////////////////////////////////////////////////////////
	// sessions details CRUD
	public SessionDetails CreateSessionDetails(long SessionsId, long TagID, long TagOrder) {
		try {
			ContentValues values = new ContentValues();
			values.put(MySQLiteHelper.COL_SessionDetails_SessionId, SessionsId);
			values.put(MySQLiteHelper.COL_SessionDetails_TagId, TagID);
			values.put(MySQLiteHelper.COL_SessionDetails_TagOrder, TagOrder);

			long insertId = database.insert(MySQLiteHelper.TBL_SessionDetails, null, values);

			Cursor cursor = database.query(MySQLiteHelper.TBL_SessionDetails, sessionDetailsColumns,
					MySQLiteHelper.COL_SessionDetails_ID + " = " + insertId, null, null, null, null);
			cursor.moveToFirst();
			SessionDetails newSession = cursorToSessionDetails(cursor);
			cursor.close();
			return newSession;

		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}

	public SessionDetails getSessionDetailsBySessionID(Long sessionId) {
		try {
			SessionDetails sessionDetails = new SessionDetails();
			Cursor cursor = database.query(MySQLiteHelper.TBL_SessionDetails, sessionsColumns,
					" (" + MySQLiteHelper.COL_SessionDetails_SessionId + " = " + sessionId + " ) ", null, null, null,
					MySQLiteHelper.COL_SessionDetails_TagOrder + " ASC");
			cursor.moveToFirst();
			while (!cursor.isAfterLast()) {
				sessionDetails = cursorToSessionDetails(cursor);
			}
			cursor.close();
			return sessionDetails;

		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}

	public void deleteSessionDetails(long ID) {

		Log.i("Delete", "Session Details deleted with id: " + ID);
		database.delete(MySQLiteHelper.TBL_SessionDetails, MySQLiteHelper.COL_SessionDetails_ID + " = " + ID, null);
	}

	// Dynamic function to update Messages Status
	public Integer UpdateSessionDetails(Long ID, long SessionsId, long TagID, long TagOrder) {
		ContentValues values = new ContentValues();
		values.put(MySQLiteHelper.COL_SessionDetails_SessionId, SessionsId);
		values.put(MySQLiteHelper.COL_SessionDetails_TagId, TagID);
		values.put(MySQLiteHelper.COL_SessionDetails_TagOrder, TagOrder);

		int rowupdated = database.update(MySQLiteHelper.TBL_SessionDetails, values,
				MySQLiteHelper.COL_SessionDetails_ID + "=" + ID, null);
		return rowupdated;
	}

	private SessionDetails cursorToSessionDetails(Cursor cursor) {
		SessionDetails sessiondetails = new SessionDetails();
		sessiondetails.setId(cursor.getLong(0));
		sessiondetails.setTagId(cursor.getLong(1));
		sessiondetails.setSessionId(cursor.getLong(2));
		sessiondetails.setTagOrder(cursor.getLong(3));
		return sessiondetails;
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Transactions CRUD

	public Transactions CreateTransactions(String Notes, long SessionId, long StepId, long UserId, long TransDateTime) {
		try {
			ContentValues values = new ContentValues();
			values.put(MySQLiteHelper.COL_Transactions_Notes, Notes);
			values.put(MySQLiteHelper.COL_Transactions_SessionId, SessionId);
			values.put(MySQLiteHelper.COL_Transactions_UserID, StepId);
			values.put(MySQLiteHelper.COL_Transactions_UserID, UserId);
			values.put(MySQLiteHelper.COL_Transactions_TransDateTime, TransDateTime);

			long insertId = database.insert(MySQLiteHelper.TBL_Transactions, null, values);

			Cursor cursor = database.query(MySQLiteHelper.TBL_Transactions, transactionsColumns,
					MySQLiteHelper.COL_Transactions_ID + " = " + insertId, null, null, null, null);
			cursor.moveToFirst();
			Transactions newtrans = cursorToTransactions(cursor);
			cursor.close();
			return newtrans;

		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}

	public List<Transactions> getTransactionsBySessionID(Long sessionId) {
		try {
			List<Transactions> transactions = new ArrayList<Transactions>();
			Cursor cursor = database.query(MySQLiteHelper.TBL_Transactions, transactionsColumns,
					" (" + MySQLiteHelper.COL_Transactions_SessionId + " = " + sessionId + " ) ", null, null, null,
					MySQLiteHelper.COL_SessionDetails_TagOrder + " ASC");

			cursor.moveToFirst();
			while (!cursor.isAfterLast()) {
				Transactions msg = cursorToTransactions(cursor);
				transactions.add(msg);
				cursor.moveToNext();
			}
			cursor.close();
			return transactions;

		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}

	public void deleteTransactions(long ID) {

		Log.i("Delete", "Transaction deleted with id: " + ID);
		database.delete(MySQLiteHelper.TBL_Transactions, MySQLiteHelper.COL_Transactions_ID + " = " + ID, null);
	}

	// Dynamic function to update Messages Status
	public Integer UpdateSessionDetails(Long ID, String Notes, long SessionId, long StepId, long UserId,
			long TransDateTime) {
		ContentValues values = new ContentValues();
		values.put(MySQLiteHelper.COL_Transactions_Notes, Notes);
		values.put(MySQLiteHelper.COL_Transactions_SessionId, SessionId);
		values.put(MySQLiteHelper.COL_Transactions_UserID, StepId);
		values.put(MySQLiteHelper.COL_Transactions_UserID, UserId);
		values.put(MySQLiteHelper.COL_Transactions_TransDateTime, TransDateTime);

		int rowupdated = database.update(MySQLiteHelper.TBL_Transactions, values,
				MySQLiteHelper.COL_Transactions_ID + "=" + ID, null);
		return rowupdated;
	}

	private Transactions cursorToTransactions(Cursor cursor) {
		Transactions transaction = new Transactions();
		transaction.setId(cursor.getLong(0));
		transaction.setNotes(cursor.getString(1));
		transaction.setSessionId(cursor.getLong(2));
		transaction.setStepId(cursor.getLong(3));
		transaction.setTransDateTime(cursor.getLong(4));
		transaction.setUserId(cursor.getLong(5));
		return transaction;
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Transactions CRUD

	public Users CreateUser(String UserName, String Password, String DisplayName, long IsAdmin, long PhoneNumber) {
		try {
			ContentValues values = new ContentValues();
			values.put(MySQLiteHelper.COL_Users_Username, UserName);
			values.put(MySQLiteHelper.COL_Users_Password, Password);
			values.put(MySQLiteHelper.COL_Users_DisplayName, DisplayName);
			values.put(MySQLiteHelper.COL_Users_IsAdmin, IsAdmin);
			values.put(MySQLiteHelper.COL_Users_PhoneNumber, PhoneNumber);

			long insertId = database.insert(MySQLiteHelper.TBL_Users, null, values);

			Cursor cursor = database.query(MySQLiteHelper.TBL_Users, usersColumns,
					MySQLiteHelper.COL_Users_ID + " = " + insertId, null, null, null, null);
			cursor.moveToFirst();
			Users newuser = cursorToUser(cursor);
			cursor.close();
			return newuser;

		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}

	public List<Users> getAllUsers() {
		try {
			List<Users> users = new ArrayList<Users>();
			Cursor cursor = database.query(MySQLiteHelper.TBL_Users, usersColumns, null, null, null, null, null);
			cursor.moveToFirst();
			try {
				while (!cursor.isAfterLast()) {
					Users msg = cursorToUser(cursor);
					users.add(msg);
					cursor.moveToNext();
				}

			} catch (Exception ex) {
				Log.e("getAllusers", ex.getMessage());
				return null;
			}
			cursor.close();
			return users;
		} catch (Exception ex) {
			ex.printStackTrace();
			Log.e("getAllusers", ex.getMessage());
			return null;
		}
	}

	public Users getUserByID(Long Id) {
		try {
			Users user = new Users();
			Cursor cursor = database.query(MySQLiteHelper.TBL_Users, usersColumns,
					" (" + MySQLiteHelper.COL_Users_ID + " = " + Id + " ) ", null, null, null, null);
			cursor.moveToFirst();
			while (!cursor.isAfterLast()) {
				user = cursorToUser(cursor);
			}
			cursor.close();
			return user;

		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}

	public Users getUserByCredential(String Username, String Password) {
		try {
			Users user = new Users();
			Cursor cursor = database.query(
					MySQLiteHelper.TBL_Users, usersColumns, " (" + MySQLiteHelper.COL_Users_Username + " = " + Username
							+ " AND " + MySQLiteHelper.COL_Users_Password + " = " + Password + " ) ",
					null, null, null, null);
			cursor.moveToFirst();
			while (!cursor.isAfterLast()) {
				user = cursorToUser(cursor);
			}
			cursor.close();
			return user;

		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}

	public void deleteUser(long ID) {

		Log.i("Delete", "Transaction deleted with id: " + ID);
		database.delete(MySQLiteHelper.TBL_Users, MySQLiteHelper.COL_Users_ID + " = " + ID, null);
	}

	// Dynamic function to update Messages Status
	public Integer Updateuser(Long ID, String UserName, String Password, String DisplayName, long IsAdmin,
			long PhoneNumber) {
		ContentValues values = new ContentValues();
		values.put(MySQLiteHelper.COL_Users_Username, UserName);
		values.put(MySQLiteHelper.COL_Users_Password, Password);
		values.put(MySQLiteHelper.COL_Users_DisplayName, DisplayName);
		values.put(MySQLiteHelper.COL_Users_IsAdmin, IsAdmin);
		values.put(MySQLiteHelper.COL_Users_PhoneNumber, PhoneNumber);

		int rowupdated = database.update(MySQLiteHelper.TBL_Users, values, MySQLiteHelper.COL_Users_ID + "=" + ID,
				null);
		return rowupdated;
	}

	private Users cursorToUser(Cursor cursor) {
		Users user = new Users();
		user.setId(cursor.getLong(0));
		user.setDisplayName(cursor.getString(1));
		user.setIsAdmin(cursor.getLong(2));
		user.setPhoneNumber(cursor.getString(3));
		return user;
	}

}
