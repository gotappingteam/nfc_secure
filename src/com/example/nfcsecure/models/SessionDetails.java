package com.example.nfcsecure.models;

public class SessionDetails {
	private long id;
	private long tagid;
	private long tagorder;
	private long sessionid;
	
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public long getSessionId() {
		return sessionid;
	}
	
	public void setSessionId(long sessionid) {
		this.sessionid = sessionid;
	}
	
	public void setTagId(long tagid) {
		this.tagid = tagid;
	}
	
	public long getTagId() {
		return tagid;
	}


	
	public long getTagOrder() {
		return tagorder;
	}

	public void setTagOrder(long tagorder) {
		this.tagorder = tagorder;
	}
	
}
