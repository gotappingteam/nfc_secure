package com.example.nfcsecure.models;

public class Tags {
	private long id;
	private long siteid;
	private String name;
	private String content;
	private String description;
	private String uid;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getUId() {
		return uid;
	}

	public void setUId(String uid) {
		this.uid = uid;
	}
	public long getSiteId() {
		return siteid;
	}

	public void setSiteId(long siteid) {
		this.siteid = siteid;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
