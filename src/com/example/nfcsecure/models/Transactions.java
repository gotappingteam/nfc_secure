package com.example.nfcsecure.models;

public class Transactions {
	
	private long id;
	private long userid;
	private long transdatetime;
	private long sessionid;
	private long stepid;
	private String notes;
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getUserId() {
		return userid;
	}

	public void setUserId(long userid) {
		this.userid = userid;
	}
	
	public long getTransDateTime() {
		return transdatetime;
	}

	public void setTransDateTime(long transdatetime) {
		this.transdatetime = transdatetime;
	}

	public long getSessionId() {
		return sessionid;
	}

	public void setSessionId(long sessionid) {
		this.sessionid = sessionid;
	}
	
	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}
	
	public long getStepId() {
		return stepid;
	}

	public void setStepId(long stepid) {
		this.stepid = stepid;
	}
}
