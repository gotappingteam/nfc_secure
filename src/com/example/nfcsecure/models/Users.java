package com.example.nfcsecure.models;

public class Users {
	private long id;
	//private long userid;
	private long isadmin;

	private String userName;
	private String password;
	private String displayname;
	private String phonenumber;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
/*
	public long getUserId() {
		return userid;
	}

	public void setUserId(long userid) {
		this.userid = userid;
	}
	*/

	public String getDisplayName() {
		return displayname;
	}

	public void setDisplayName(String displayname) {
		this.displayname = displayname;
	}
	
	public String getPhoneNumber() {
		return phonenumber;
	}

	public void setPhoneNumber(String phonenumber) {
		this.phonenumber = phonenumber;
	}	 
	
	public long getIsAdmin() {
		return isadmin;
	}

	public void setIsAdmin(long isadmin) {
		this.isadmin = isadmin;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
