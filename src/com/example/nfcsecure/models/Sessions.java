package com.example.nfcsecure.models;

public class Sessions {
	
	private long id;
	private long siteid;
	private long isenabled;
	private String days;
	private long starttime;
	private long duration;
	private String description;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getSiteId() {
		return siteid;
	}
	
	public void setIsEnabled(long isenabled) {
		this.isenabled = isenabled;
	}

	public long getIsEnabled() {
		return isenabled;
	}

	public String getDays() {
		return days;
	}

	public void setDays(String days) {
		this.days = days;
	}
	
	public long getStartTime() {
		return starttime;
	}

	public void setStartTime(long starttime) {
		this.starttime = starttime;
	}
	
	public long getDuration() {
		return duration;
	}

	public void setDuration(long duration) {
		this.duration = duration;
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
