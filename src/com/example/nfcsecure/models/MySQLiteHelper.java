package com.example.nfcsecure.models;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class MySQLiteHelper extends SQLiteOpenHelper {

	// Users table
	public static final String TBL_Users = "users";
	public static final String COL_Users_ID = "_id";
	public static final String COL_Users_IsAdmin = "isadmin";
	public static final String COL_Users_Username = "username";
	public static final String COL_Users_Password = "password";
	public static final String COL_Users_DisplayName = "displayname";
	public static final String COL_Users_PhoneNumber = "phonenumber";

	// Sessions table
	public static final String TBL_Sessions = "sessions";
	public static final String COL_Sessions_ID = "_id";
	public static final String COL_Sessions_IsEnabled = "isenabled";
	public static final String COL_Sessions_Description = "description";
	public static final String COL_Sessions_Duration = "duration";
	public static final String COL_Sessions_SiteId = "siteid";
	public static final String COL_Sessions_StartTime = "starttime";
	public static final String COL_Sessions_Days = "days";

	// Sessions table
	public static final String TBL_SessionDetails = "sessiondetails";
	public static final String COL_SessionDetails_ID = "_id";
	public static final String COL_SessionDetails_TagId = "tagid";
	public static final String COL_SessionDetails_TagOrder = "tagorder";
	public static final String COL_SessionDetails_SessionId = "sessionid";

	// Sites table
	public static final String TBL_Sites = "sites";
	public static final String COL_Sites_ID = "_id";
	public static final String COL_Sites_Code = "code";
	public static final String COL_Sites_Name = "name";

	// Tags table
	public static final String TBL_Tags = "tags";
	public static final String COL_Tags_ID = "_id";
	public static final String COL_Tags_SiteID = "siteid";
	public static final String COL_Tags_Name = "name";
	public static final String COL_Tags_Content = "content";
	public static final String COL_Tags_description = "description";
	public static final String COL_Tags_UId = "uid";

	// Transactions
	public static final String TBL_Transactions = "transactions";
	public static final String COL_Transactions_ID = "_id";
	public static final String COL_Transactions_UserID = "userid";
	public static final String COL_Transactions_StepID = "stepid";
	public static final String COL_Transactions_TransDateTime = "transdatetime";
	public static final String COL_Transactions_Notes = "notes";
	public static final String COL_Transactions_SessionId = "sessionid";

	private static final String DATABASE_NAME = "nfcSecurity.db";
	private static final int DATABASE_VERSION = 1;
	// Version 3 (New table of Users)

	// Database creation sql statement
	private static final String CREATE_TABLE_Users = "create table " + TBL_Users + "(" + COL_Users_ID
			+ " integer primary key autoincrement, " + COL_Users_IsAdmin + " INTEGER not null ," + COL_Users_PhoneNumber
			+ " INTEGER not null ," + COL_Users_DisplayName + " text not null  ," + COL_Users_Username
			+ " text not null  ," + COL_Users_Password + " text not null  );";

	// Database creation sql statement
	private static final String CREATE_TABLE_Sites = "create table " + TBL_Sites + "(" + COL_Sites_ID
			+ " integer primary key autoincrement, " + COL_Sites_Name + " text not null ," + COL_Sites_Code
			+ " INTEGER not null );";

	// Database creation sql statement
	private static final String CREATE_TABLE_Tags = "create table " + TBL_Tags + "(" + COL_Tags_ID
			+ " integer primary key autoincrement, " + COL_Tags_UId + " text not null ," + COL_Tags_Content + " text, "
			+ COL_Tags_Name + " text,  " + COL_Tags_description + " text , " + COL_Tags_SiteID + " integer not null );";

	// Database creation sql statement
	private static final String CREATE_TABLE_Sessions = "create table " + TBL_Sessions + "(" + COL_Sessions_ID
			+ " integer primary key autoincrement, " + COL_Sessions_SiteId + " integer not null ," + COL_Sessions_Days
			+ " text not null , " + COL_Sessions_Description + " text,  " + COL_Sessions_StartTime + " integer , "
			+ COL_Sessions_Duration + " integer ,  " + COL_Sessions_IsEnabled + " integer not null );";

	// Database creation sql statement
	private static final String CREATE_TABLE_SessionDetails = "create table " + TBL_SessionDetails + "("
			+ COL_SessionDetails_ID + " integer primary key autoincrement, " + COL_SessionDetails_TagId
			+ " integer not null ," + COL_SessionDetails_TagOrder + " integer not null );";

	private static final String CREATE_TABLE_Transactions = "create table " + TBL_Transactions + "("
			+ COL_Transactions_ID + " integer primary key autoincrement, " + COL_Transactions_UserID
			+ " INTEGER not null ," + COL_Transactions_SessionId + " INTEGER not null ," + COL_Transactions_StepID
			+ " INTEGER not null  ," + COL_Transactions_TransDateTime + " INTEGER not null ," + COL_Transactions_Notes
			+ " text );";

	public MySQLiteHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		db.execSQL(CREATE_TABLE_Users);
		db.execSQL(CREATE_TABLE_Sites);
		db.execSQL(CREATE_TABLE_Tags);
		db.execSQL(CREATE_TABLE_Sessions);
		db.execSQL(CREATE_TABLE_SessionDetails);
		db.execSQL(CREATE_TABLE_Transactions);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub

		Log.w(MySQLiteHelper.class.getName(), "Upgrading database from version " + oldVersion + " to " + newVersion
				+ ", which will destroy all old data");
		db.execSQL("DROP TABLE IF EXISTS " + TBL_Users);
		db.execSQL("DROP TABLE IF EXISTS " + TBL_Sites);
		db.execSQL("DROP TABLE IF EXISTS " + TBL_Tags);
		db.execSQL("DROP TABLE IF EXISTS " + TBL_Sessions);
		db.execSQL("DROP TABLE IF EXISTS " + TBL_SessionDetails);
		db.execSQL("DROP TABLE IF EXISTS " + TBL_Transactions);

		onCreate(db);

	}
}
